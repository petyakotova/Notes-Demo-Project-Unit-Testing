﻿using Notes_UnitTesting_Sample.Models;
using System.Data.Entity;

namespace Notes_UnitTesting_Sample.DAL
{
    public class NotesContext : DbContext
    {
        public NotesContext() 
            : base(@"Data Source = DESKTOP-5MR17K6; Initial Catalog = NotesDb; Integrated Security = True")
        {

        }

        public DbSet<Note> Notes { get; set; }
    }
}