﻿
namespace Notes_UnitTesting_Sample.Models
{
    public class Note
    {
        public int Id { get; set; }
        public string  Title { get; set; }
        public string  Content { get; set; }
    }
}