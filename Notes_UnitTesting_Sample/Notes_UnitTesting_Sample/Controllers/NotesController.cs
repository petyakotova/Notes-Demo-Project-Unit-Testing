﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Notes_UnitTesting_Sample.DAL;
using Notes_UnitTesting_Sample.Models;

namespace Notes_UnitTesting_Sample.Controllers
{
    public class NotesController : ApiController
    {
        List<Note> notes = new List<Note>();
        private NotesContext context = new NotesContext();

        public NotesController() { }

        public NotesController(List<Note> notes)
        {
            this.notes = notes;
        }
        // GET: api/Notes
        public IEnumerable<Note> GetAllNotes()
        {
            return notes;
        }

        // GET: api/Notes/5
        [ResponseType(typeof(Note))]
        public HttpResponseMessage Get(int id)
        {
            var employee = context.Notes.Where(p => p.Id == id)
                .FirstOrDefault();
            if (employee == null)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }
            return Request.CreateResponse(HttpStatusCode.OK, employee);
        }
        // PUT: api/Notes/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutNote(int id, Note note)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != note.Id)
            {
                return BadRequest();
            }

            context.Entry(note).State = EntityState.Modified;

            try
            {
                context.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!NoteExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Notes
        [ResponseType(typeof(Note))]
        public IHttpActionResult PostNote(Note note)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            context.Notes.Add(note);
            context.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = note.Id }, note);
        }

        // DELETE: api/Notes/5
        [ResponseType(typeof(Note))]
        public IHttpActionResult DeleteNote(int id)
        {
            Note note = context.Notes.Find(id);
            if (note == null)
            {
                return NotFound();
            }

            context.Notes.Remove(note);
            context.SaveChanges();

            return Ok(note);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                context.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool NoteExists(int id)
        {
            return context.Notes.Count(e => e.Id == id) > 0;
        }
    }
}