﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Notes_UnitTesting_Sample.Controllers;
using Notes_UnitTesting_Sample.Models;
using System.Web.Http.Results;
using System.Net.Http;
using System.Web.Http;
using System.Collections.Generic;

namespace Tests
{
    [TestClass]
    public class NotesController_Tests
    {
        [TestMethod]
        public void GetByIdTest()
        {
            // Set up    
            var controller = new NotesController();
            controller.Request = new HttpRequestMessage();
            controller.Configuration = new HttpConfiguration();

            // Act on Test  
            var response = controller.Get(2);

            // Assert the result  
            Note note = new Note();
            Assert.IsTrue(response.TryGetContentValue<Note>(out note));
        }

        [TestMethod]
        public void GetAllTest()
        {
            // Set up    
            var testProducts = GetTestNote();
            var controller = new NotesController(testProducts);

            // Act on Test  
            var result = controller.GetAllNotes() as List<Note>;

            // Assert the result  
            Assert.AreEqual(testProducts.Count, result.Count);
        }

        private List<Note> GetTestNote()
        {
            var testProducts = new List<Note>();

            testProducts.Add(new Note { Id = 1, Title = "Example1", Content = "This is first example" });
            testProducts.Add(new Note { Id = 1, Title = "Example2", Content = "This is second example" });

            return testProducts;
        }
    }
}
